from game import *
import numpy as np
import time

Q=np.zeros((81,4))
gamma=0.8#discout factor
alpha=0.1#learning rate

def reward(game,moved):#reward function
	id=game.getCurrentID()
	offset=0
	if not moved:#bad reward if it don't move
		offset=-100
	if id==2:#bad reward for red case
		return -20+offset
	elif id==3:#good reward for green case
		return 100+offset
	return 0

def randargmax(lst):
	max=np.max(lst)
	argMax=[]
	for i,v in enumerate(lst):
		if v==max:
			argMax.append(i)
	return np.random.choice(argMax)#pick randomly if there are more than 1

def episode(game,Q,random,max=100,wait=0.0):
	game.reset()#restart the game
	[nx,ny]=game.getStat()
	for i in range(max):#process
		if wait>0.0:
			time.sleep(wait)
		[x,y]=[nx,ny]
		state=x+y*8
		if np.random.rand(1)<random:#action random
			a=np.random.randint(4)
		else:#one of the best options
			a=randargmax(Q[state,:])
		moved=game.move(a)#next step
		[nx,ny]=game.getStat()
		nState=nx+ny*8
		Q[state,a]=Q[state,a]+alpha*(reward(game,moved)+gamma*np.max(Q[nState,:])-Q[state,a])#bellman equation
		if game.getCurrentID()==3:#goal of the bot
			return Q,True
	return Q,False

game=Game()
game.start()
while not game.isReady():
	time.sleep(0.1)
Q,find=episode(game,Q,random=1.0,max=50,wait=0.01)#start with random action only
e,validation=1,0
while validation<20:#waiting for 20 success
	print('episode:',e)
	Q,find=episode(game,Q,random=0.3,max=50,wait=0.01)
	if find:
		validation+=1
	else:
		validation=0
	e+=1
print('find!')
time.sleep(3)
episode(game,Q,random=0.0,max=50,wait=0.5)#replay of the result
input('press Enter to continue...')
game.stop()
