from tkinter import *
from threading import Thread
import time

class Game(Thread):
	def __init__(self):
		Thread.__init__(self)
                #define the game
		self.map=[[0,0,0,2,0,0,0,0],
			[0,3,0,0,2,0,0,0],
			[0,0,0,0,1,0,0,0],
			[0,2,2,0,1,0,0,0],
			[0,0,0,0,1,0,0,0],
			[0,0,0,0,0,0,0,0],
			[0,0,0,0,0,0,0,0],
			[0,0,0,0,0,0,0,0]]
		self.width=8
		self.height=8
		self.player={'x':7,'y':4}
		self.init=False
	
	def stop(self):
		self.window.quit()
	
	def getStat(self):
		return [self.player['x'],self.player['y']]

	def isReady(self):
		return self.init
	
	'''
	* direction:
	* 0=north
	* 1=est
	* 2=south
	* 3=west
	'''
	def move(self,direction):
		x,y,moved=self.player['x'],self.player['y'],False
		if direction==0 and y>0 and self.map[y-1][x]!=1:#check if it can move
			y-=1#move
			self.canvas.move(self.player['rect'],0,-100)
			moved=True
		elif direction==1 and x<7 and self.map[y][x+1]!=1:#check if it can move
			x+=1#move
			self.canvas.move(self.player['rect'],100,0)
			moved=True
		elif direction==2 and y<7 and self.map[y+1][x]!=1:#check if it can move
			y+=1#move
			self.canvas.move(self.player['rect'],0,100)
			moved=True
		elif direction==3 and x>0 and self.map[y][x-1]!=1:#check if it can move
			x-=1#move
			self.canvas.move(self.player['rect'],-100,0)
			moved=True
		self.player['x'],self.player['y']=x,y#update position
		return moved

	def getCurrentID(self):
		return self.map[self.player['y']][self.player['x']]

	def reset(self):
		x,y=7,4
		self.player['x'],self.player['y']=x,y
		self.canvas.coords(self.player['rect'],x*100+25,y*100+25,x*100+75,y*100+75)

	def run(self):
                #config tkinter
		self.window=Tk()
		self.canvas=Canvas(self.window,width=800,height=800)
		self.canvas.pack()
		for y,line in enumerate(self.map):
			for x,id in enumerate(line):
				if id==0:
					self.canvas.create_rectangle(x*100+1,y*100+1,x*100+100,y*100+100)
				elif id==1:
					self.canvas.create_rectangle(x*100+1,y*100+1,x*100+100,y*100+100,fill="gray")
				elif id==2:
					self.canvas.create_rectangle(x*100+1,y*100+1,x*100+100,y*100+100,fill="red")
				elif id==3:
					self.canvas.create_rectangle(x*100+1,y*100+1,x*100+100,y*100+100,fill="green")
				else:
					self.canvas.create_rectangle(x*100+1,y*100+1,y*100+100,y*100+100,fill="pink")
		self.player['rect']=self.canvas.create_rectangle(self.player['x']*100+25,self.player['y']*100+25,self.player['x']*100+75,self.player['y']*100+75,fill="black")
		self.init=True
                #let it run
		self.window.mainloop()

